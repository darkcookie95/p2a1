/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

/**
 *
 * @author ori16108422
 */
public class SQLInsert {

    public final static String INSERT = "INSERT INTO ";

    public final static String EMPLOYEE = INSERT + "tblEmployee (FirstName, Surname, "
            + "DOB, Address1, Address2, City, Postcode, PhoneNo, Email, "
            + "PayType, ContractHours, RateOfPay, MaxHoliday, CurHoliday, "
            + "MaxSick, CurSick, Active) VALUES (?, ?, ?, ?, ?, ?, ?, "
            + "?, ?, ?, ?, ?, ?, 0, ?, 0, true)";

    public final static String HOLIDAY = INSERT + "tblHolidayRequest (EmployeeID, "
            + "HolidayDate, Notes, Status) VALUES (?, ?, ?, 'Pending')";

    public final static String APPROVED_HOLIDAY = INSERT + "tblApprovedHoliday "
            + "(EmployeeID, HoldiayDate, Processed) VALUES (?, ?, false)";

    public final static String ROTA = INSERT + "tblRota (EmployeeID, ShiftDate, "
            + "StartTime, EndTime) VALUES (?, ?, ?, ?)";

    public final static String CLOCK_IN = INSERT + "tblClockIn (EmployeeID, ClockIn) "
            + "VALUES (?, CURRENT_TIMESTAMP)";

    public final static String PAYSLIP = INSERT + "tblPayslip (EmployeeID, TotalHours, "
            + "GrossPay, NationalInsurance, TaxBracket1, TaxBracket2, TaxBracket3, "
            + "ProcessedOn) VALUES (?, ?, ?, ?, ?, ?, ?, GETDATE())";
    
    
}
