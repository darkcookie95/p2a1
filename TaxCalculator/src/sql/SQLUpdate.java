/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

/**
 *
 * @author ori16108422
 */
public class SQLUpdate {

    public final static String EMPLOYEE = "UPDATE tblEmployee SET FirstName = ?, "
            + "Surname = ?, DOB = ?, Address1 = ?, Address2 = ?, City = ?, Postcode = ?, "
            + "PhoneNo = ?, Email = ?, PayType = ?, ContractHours = ?, "
            + "RateOfPay = ?, MaxHoliday = ?, MaxSick = ? WHERE EmployeeID = ?";
    
    public final static String PASSWORD = "UPDATE tblEmployee SET Password = ? WHERE EmployeeID = ?";

    public final static String HOLIDAY = "UPDATE tblHolidayRequest SET Status = ? "
            + "WHERE RequestID = ?";
    
    public static final String APPROVED_HOLIDAY = "UPDATE tblEmployee SET CurHoliday = ? "
            + "WHERE EmployeeID = ?";

    public final static String PROCESS_HOLIDAY = "UPDATE tblApprovedHoliday SET "
            + "Processed = true WHERE HolidayID = ?";

    public final static String ROTA = "UPDATE tblRota SET employeeID = ?, ShiftDate = ?, "
            + "StartTime = ?, EndTime = ? WHERE RotaID = ?";

    public final static String CLOCK_OUT = "UPDATE tblClockIn SET ClockOut = CURRENT_TIMESTAMP "
            + "WHERE ClockID = (SELECT EmployeeID FROM tblClock-In/Out WHERE ClockID = "
            + "(SELECT MAX(ClockID) FROM tblClock-In/Out AND WHERE EmployeeID = ?) "
            + "AND EmployeeID = ?";
    public final static String PROCESS_CLOCK = "UPDATE tblClockIn SET Processed = true "
            + "WHERE EmployeeID = ?";

}
