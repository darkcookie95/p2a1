/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

/**
 *
 * @author ori16108422
 */
public class SQLSelect {

    public final static String SELECT = "SELECT * FROM ";
    
    public final static String EMPLOYEE = SELECT + "tblEmployee WHERE Active = true";
    public final static String EMPLOYEE_BY_NAME = EMPLOYEE + " AND SName = ?";
    public final static String INDIVIDUAL_EMPLOYEE = EMPLOYEE + " WHERE EmployeeId = ?";
    
    public final static String HOLIDAY = SELECT + "tblHolidayRequest";
    public final static String PENDING_HOLIDAY = HOLIDAY + " WHERE Status = 'Pending'";
    public final static String INDIVIDUAL_HOLIDAY = HOLIDAY + " WHERE EmployeeID = ?";
    public final static String APPROVED_HOLIDAY = SELECT + "tblApprovedHoliday";
    
    public final static String ROTA = SELECT + "tblRota";
    public final static String INDIVIDUAL_ROTA = ROTA + " WHERE EmplyeeID = ?";
    
    public final static String CLOCK_IN = SELECT + "tblClockIn";
    public final static String INDIVIDUAL_CLOCK_IN = CLOCK_IN + " WHERE employeeID = ? "
            + "AND processed = false";
    
    public final static String PAYSLIP = SELECT + "tblPayslip";
    public final static String INDIVIDUAL_PAYSLIP = PAYSLIP + " WHERE EmployeeID = ?";


}
