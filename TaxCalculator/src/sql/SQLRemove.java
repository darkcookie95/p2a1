/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

/**
 *
 * @author ori16108422
 */
public class SQLRemove {

    public final static String EMPLOYEE = "UPDATE tblEmployee SET Active = false WHERE EmployeeID = ?";

    public final static String HOLIDAY_REQUEST = "DELETE FROM tblHollidayRequest WHERE RequestID = ?";

    public final static String ROTA = "DELETE FROM tblRota WHERE RotaID = ?";
}
