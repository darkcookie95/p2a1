/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author ori16108422
 */
public class TaxBrackets {
 
    final static int NATIONAL_INSURANCE_THRESHOLD = 8632;
    final static int NATIONAL_INSURANCE_HIGHER_THRESHOLD = 50024;
    final static int PERSONAL_ALLOWANCE_THRESHOLD = 12500;
    final static int BASIC_RATE_THRESHOLD = 50000;
    final static int HIGHER_RATE_THRESHOLD = 150000;
    
    final static double NATIONAL_INSURANCE_PERCENT = 0.12;
    final static double NATIONAL_INSURANCE_HIGHER_PERCENT = 0.02;
    final static double BASIC_RATE_PERCENT = 0.2;
    final static double HIGHER_RATE_PERCENT = 0.4;
    final static double ADDITIONAL_RATE_PERCENT = 0.45;
}
