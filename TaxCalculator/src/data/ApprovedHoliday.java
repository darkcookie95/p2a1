/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Date;
import java.sql.SQLException;
import sql.*;

/**
 *
 * @author ori16108422
 */
public class ApprovedHoliday {

    private int holidayID;
    private int employeeID;
    private String holidayDate;
    private boolean processed;

    public ApprovedHoliday(int holidayID, int employeeID, String holidayDate, boolean processed) {
        this.holidayID = holidayID;
        this.employeeID = employeeID;
        this.holidayDate = holidayDate;
        this.processed = processed;
    }

    public int getHolidayID() {
        return holidayID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(String holidayDate) {
        this.holidayDate = holidayDate;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public static void addApprovedHoliday(int employeeID, String holidayDate) {
        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.APPROVED_HOLIDAY);

            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.setString(2, holidayDate);

            DBConnection.prepStatement.executeUpdate();
            
            DBConnection.getAllApprovedHoliday();

        } catch (SQLException sqlex) {

        }
    }
}
