/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;
import java.util.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sql.SQLInsert;
import sql.SQLRemove;
import sql.SQLUpdate;
/**
 *
 * @author ori16108422
 */
public class HolidayRequest {
    private int requestID;
    private int employeeID;
    private String holidayDate;
    private String notes;
    private String status;

    public HolidayRequest(int requestID, int employeeID, String holidayDate, String notes, String status) {
        this.requestID = requestID;
        this.employeeID = employeeID;
        this.holidayDate = holidayDate;
        this.notes = notes;
        this.status = status;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(String holidayDate) {
        this.holidayDate = holidayDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static void addHolidayRequest(int employeeID, String date, String note){
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.HOLIDAY);
            
            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.setString(2, date);
            DBConnection.prepStatement.setString(3, note);
            
            DBConnection.prepStatement.executeQuery();
            
            DBConnection.getIndividualHolidayRequest(employeeID);
        } catch (SQLException ex) {
            Logger.getLogger(HolidayRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void removeHoliday(int ID, int employeeID){
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLRemove.HOLIDAY_REQUEST);
            
            DBConnection.prepStatement.setInt(1, ID);
            
            DBConnection.prepStatement.executeQuery();
            
            DBConnection.getIndividualHolidayRequest(employeeID);
        } catch (SQLException ex) {
            Logger.getLogger(HolidayRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void approved(int ID){
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.HOLIDAY);
            
            DBConnection.prepStatement.setString(1, "Approved");
            DBConnection.prepStatement.setInt(2, ID);
            
            DBConnection.prepStatement.executeQuery();
            
            DBConnection.getAllHolidayRequest();
        } catch (SQLException ex) {
            Logger.getLogger(HolidayRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void rejected(int ID){
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.HOLIDAY);
            
            DBConnection.prepStatement.setString(1, "Rejected");
            DBConnection.prepStatement.setInt(2, ID);
            
            DBConnection.prepStatement.executeQuery();
            
            DBConnection.getAllHolidayRequest();
        } catch (SQLException ex) {
            Logger.getLogger(HolidayRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
