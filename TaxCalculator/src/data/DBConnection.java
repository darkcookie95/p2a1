/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ucanaccess.jdbc.UcanaccessDriver;
import sql.SQLSelect;

/**
 *
 * @author ori16108422
 */
public class DBConnection {

    //Variable Declaration
    private static final String DATABASE_NAME = "DB.accdb";

    /*Variable to identify currently logged in user*/
    public static Employee currentUser;

    /*Variables to be used when executing SQL queries*/
    public static Connection connection = null;
    public static PreparedStatement prepStatement = null;
    public static ResultSet resultSet = null;

    /*ArrayLists to store information pulled from the database*/
    public static ArrayList<Employee> employeeArray = new ArrayList<>();
    public static ArrayList<HolidayRequest> holidayRequestArray = new ArrayList<>();
    public static ArrayList<ApprovedHoliday> approvedHolidayArray = new ArrayList<>();
    public static ArrayList<Rota> rotaArray = new ArrayList<>();
    public static ArrayList<ClockIn> clockInArray = new ArrayList<>();
    public static ArrayList<Payslip> payslipArray = new ArrayList<>();
    //END of Variable Declaration

    /**
     * Method to establish connection with the database
     */
    public static void connectToDB() {
        try {

            File file = new File(DATABASE_NAME);
            connection = DriverManager.getConnection(UcanaccessDriver.URL_PREFIX + file.getAbsolutePath() + ";");
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        } //try catch END
    }//connectToDB() end

    /**
     * Method to close connection to the database
     */
    public static void closeDB() {
        try {
            connection.close();
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        }//try catch END
    }//closeDB() END

    /**
     * Getter to identify currently logged in user
     *
     * @return
     */
    public static Employee getCurrentUser() {
        return currentUser;
    }

    /**
     * Setter to set currently logged in user
     *
     * @param currentUser
     */
    public static void setCurrentUser(Employee currentUser) {
        DBConnection.currentUser = currentUser;
    }

    /**
     * Method to populate employeeArray with information pulled from the
     * database
     */
    public static void getAllEmployees() {
        if (employeeArray != null) {
            employeeArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement("SELECT * FROM tblEmployee;");
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {

                int employeeID = resultSet.getInt("EmployeeID");
                String employeeFName = resultSet.getString("FirstName");
                String employeeSName = resultSet.getString("Surname");
                String employeeDOB = resultSet.getString("DOB");
                String employeeAddress1 = resultSet.getString("Address1");
                String employeeAddress2 = resultSet.getString("Address2");
                String employeeCity = resultSet.getString("City");
                String employeePostcode = resultSet.getString("Postcode");
                String employeePhone = resultSet.getString("PhoneNo");
                String employeeEmail = resultSet.getString("Email");
                String employeePayType = resultSet.getString("PayType");
                int employeeContractHours = resultSet.getInt("ContractHours");
                double employeePayRate = resultSet.getDouble("RateOfPay");
                int employeeMaxHoliday = resultSet.getInt("MaxHoliday");
                int employeeCurHoliday = resultSet.getInt("CurHoliday");
                int employeeMaxSick = resultSet.getInt("MaxSick");
                int employeeCurSick = resultSet.getInt("CurSick");
                String employeePassword = resultSet.getString("Password");
                String employeeAccessLevel = resultSet.getString("AccessLevel");
                boolean employeeActive = resultSet.getBoolean("Active");

                //instantiate new employee object from data
                employeeArray.add(new Employee(employeeID, employeeFName, employeeSName,
                        employeeDOB, employeeAddress1, employeeAddress2, employeeCity,
                        employeePostcode, employeePhone, employeeEmail, employeePayType,
                        employeeContractHours, employeePayRate, employeeMaxHoliday,
                        employeeCurHoliday, employeeMaxSick, employeeCurSick,
                        employeePassword, employeeAccessLevel, employeeActive));

            }//while END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END
    }//getAllEmployees() END

    /**
     * Method to populate employeeArray with information pulled from the
     * database if the surname matches the parameter
     *
     * @param Surname
     */
    public static void getEmployeeBySurname(String Surname) {

        if (employeeArray != null) {
            employeeArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.EMPLOYEE_BY_NAME);
            prepStatement.setString(1, Surname);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int employeeID = resultSet.getInt("EmployeeID");
                String employeeFName = resultSet.getString("FirstName");
                String employeeSName = resultSet.getString("LastName");
                String employeeDOB = resultSet.getString("DOB");
                String employeeAddress1 = resultSet.getString("Address1");
                String employeeAddress2 = resultSet.getString("Address2");
                String employeeCity = resultSet.getString("City");
                String employeePostcode = resultSet.getString("Postcode");
                String employeePhone = resultSet.getString("PhoneNo");
                String employeeEmail = resultSet.getString("Email");
                String employeePayType = resultSet.getString("PayType");
                int employeeContractHours = resultSet.getInt("ContractHours");
                double employeePayRate = resultSet.getDouble("RateOfPay");
                int employeeMaxHoliday = resultSet.getInt("MaxHoliday");
                int employeeCurHoliday = resultSet.getInt("CurHoliday");
                int employeeMaxSick = resultSet.getInt("MaxSick");
                int employeeCurSick = resultSet.getInt("CurSick");
                String employeePassword = resultSet.getString("Password");
                String employeeAccessLevel = resultSet.getString("AccessLevel");
                boolean employeeActive = resultSet.getBoolean("Active");

                //instantiate new employee object from data
                employeeArray.add(new Employee(employeeID, employeeFName, employeeSName,
                        employeeDOB, employeeAddress1, employeeAddress2, employeeCity, employeePostcode,
                        employeePhone, employeeEmail, employeePayType, employeeContractHours,
                        employeePayRate, employeeMaxHoliday, employeeCurHoliday, employeeMaxSick,
                        employeeCurSick, employeePassword, employeeAccessLevel, employeeActive));
            }

        } catch (SQLException sqlex) {

        } catch (Exception ex) {

        }//try catch END

    }//getEmployeeByName() END

    public static void getAllHolidayRequest() {
        if (holidayRequestArray != null) {
            holidayRequestArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.PENDING_HOLIDAY);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int requestID = resultSet.getInt("RequestID");
                int empID = resultSet.getInt("EmployeeID");
                String holidayDate = resultSet.getString("HolidayDate");
                String notes = resultSet.getString("Notes");
                String status = resultSet.getString("Status");

                //instantiate new employee object from data
                holidayRequestArray.add(new HolidayRequest(requestID, empID,
                        holidayDate, notes, status));

            }//while END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END

    }

    public static void getAllApprovedHoliday() {
        if (approvedHolidayArray != null) {
            approvedHolidayArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.APPROVED_HOLIDAY);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int holidayId = resultSet.getInt("HolidayId");
                int employeeID = resultSet.getInt("EmployeeID");
                String holidayDate = resultSet.getString("HolidayDate");
                boolean processed = resultSet.getBoolean("Processed");

                //instantiate new employee object from data
                approvedHolidayArray.add(new ApprovedHoliday(holidayId, employeeID,
                        holidayDate, processed));

            }//while END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END
    }

    public static void getAllRota() {
        if (rotaArray != null) {
            rotaArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.ROTA);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int rotaID = resultSet.getInt("RotaID");
                int employeeID = resultSet.getInt("EmployeeID");
                String shiftDate = resultSet.getString("ShiftDate");
                String startTime = resultSet.getString("StartTime");
                String endTime = resultSet.getString("EndTime");


                rotaArray.add(new Rota(rotaID, employeeID, shiftDate, startTime,
                        endTime));
            }
        } catch (SQLException sqlex) {

        }
    }

    public static void getIndividualEmployee(int employeeID) {
        if (employeeArray != null) {
            employeeArray.clear();
        }
        try {

            prepStatement = connection.prepareStatement(SQLSelect.INDIVIDUAL_EMPLOYEE);

            prepStatement.setInt(1, employeeID);

            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                String employeeFName = resultSet.getString("EmployeeFName");
                String employeeSName = resultSet.getString("EmployeeSName");
                String employeeDOB = resultSet.getString("DOB");
                String employeeAddress1 = resultSet.getString("EmployeeAddress1");
                String employeeAddress2 = resultSet.getString("EmployeeAddress2");
                String employeeCity = resultSet.getString("EmployeeCity");
                String employeePostcode = resultSet.getString("EmployeePostcode");
                String employeePhone = resultSet.getString("EmployeePhone");
                String employeeEmail = resultSet.getString("EmployeeEmail");
                String employeePayType = resultSet.getString("EmployeePayType");
                int employeeContractHours = resultSet.getInt("EmployeeContractHours");
                double employeePayRate = resultSet.getDouble("EmployeePayRate");
                int employeeMaxHoliday = resultSet.getInt("EmployeeMaxHoliday");
                int employeeCurHoliday = resultSet.getInt("EmployeeCurHoliday");
                int employeeMaxSick = resultSet.getInt("EmployeeMaxSick");
                int employeeCurSick = resultSet.getInt("EmployeeCurSick");
                String employeePassword = resultSet.getString("EmployeePassword");
                String employeeAccessLevel = resultSet.getString("EmployeeAccessLevel");
                boolean employeeActive = resultSet.getBoolean("EmployeeActive");

                employeeArray.add(new Employee(employeeID, employeeFName, employeeSName,
                        employeeDOB, employeeAddress1, employeeAddress2, employeeCity,
                        employeePostcode, employeePhone, employeeEmail, employeePayType,
                        employeeContractHours, employeePayRate, employeeMaxHoliday,
                        employeeCurHoliday, employeeMaxSick, employeeCurSick, employeePassword,
                        employeeAccessLevel, employeeActive));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getIndividualClockIn(int EmployeeID) {
        if (clockInArray != null) {
            clockInArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.INDIVIDUAL_CLOCK_IN);

            prepStatement.setInt(1, EmployeeID);

            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {

                int clockID = resultSet.getInt("ClockID");
                int employeeID = resultSet.getInt("EmployeeID");
                String clockIn = resultSet.getString("ClockIn");
                String clockOut = resultSet.getString("ClockOut");
                boolean processed = resultSet.getBoolean("Processed");

                clockInArray.add(new ClockIn(clockID, employeeID, clockIn, clockOut, processed));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void getIndividualHolidayRequest(int employeeID) {
        if (holidayRequestArray != null) {
            holidayRequestArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.INDIVIDUAL_HOLIDAY);
            prepStatement.setInt(1, employeeID);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int requestID = resultSet.getInt("RequestID");
                String holidayDate = resultSet.getString("HolidayDate");
                String notes = resultSet.getString("Notes");
                String status = resultSet.getString("Status");

                //instantiate new employee object from data
                holidayRequestArray.add(new HolidayRequest(requestID, employeeID,
                        holidayDate, notes, status));

            }//while END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END

    }

    public static void getIndividualRota(int employeeID) {
        if (rotaArray != null) {
            rotaArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.INDIVIDUAL_ROTA);
            prepStatement.setInt(1, employeeID);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int rotaID = resultSet.getInt("RotaID");
                String shiftDate = resultSet.getString("ShiftDate");
                String startTime = resultSet.getString("StartTime");
                String endTime = resultSet.getString("EndTime");


                rotaArray.add(new Rota(rotaID, employeeID, shiftDate, startTime,
                        endTime));
            }
        } catch (SQLException sqlex) {

        }
    }

    public static void getIndividualPayslip(int employeeID) {
        if (payslipArray != null) {
            payslipArray.clear();
        }

        try {

            prepStatement = connection.prepareStatement(SQLSelect.INDIVIDUAL_PAYSLIP);
            prepStatement.setInt(1, employeeID);
            resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                int payslipID = resultSet.getInt("PayslipID");
                double totalHours = resultSet.getDouble("TotalHours");
                double grossPay = resultSet.getDouble("GrossPay");
                double nationalInsurance = resultSet.getDouble("NationalInsurance");
                double taxBracket1 = resultSet.getDouble("TaxBracket1");
                double taxBracket2 = resultSet.getDouble("TaxBracket2");
                double taxBracket3 = resultSet.getDouble("TaxBracket3");
                double netPay = resultSet.getDouble("NetPay");
                Date processedOn = resultSet.getDate("ProcessedOn");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
