/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import sql.SQLInsert;
import sql.SQLRemove;
import sql.SQLUpdate;

/**
 *
 * @author ori16108422
 */
public class Employee {

    private int employeeID;
    private String employeeFName;
    private String employeeSName;
    private String employeeDOB;
    private String employeeAddress1;
    private String employeeAddress2;
    private String employeeCity;
    private String employeePostcode;
    private String employeePhone;
    private String employeeEmail;
    private String employeePayType;
    private int employeeContractHours;
    private double employeePayRate;
    private int employeeMaxHoliday;
    private int employeeCurHoliday;
    private int employeeMaxSick;
    private int employeeCurSick;
    private String employeePassword;
    private String employeeAccessLevel;
    private boolean employeeActive;

    /*
    * Employee class constructor
     */
    public Employee(int ID, String fName, String sName,
            String DOB, String address1, String address2,
            String city, String postcode, String phone,
            String email, String payType, int contractHours,
            double payRate, int maxHoliday, int curHoliday,
            int maxSick, int curSick, String password,
            String accessLevel, boolean active) {
        employeeID = ID;
        employeeFName = fName;
        employeeSName = sName;
        employeeDOB = DOB;
        employeeAddress1 = address1;
        employeeAddress2 = address2;
        employeeCity = city;
        employeePostcode = postcode;
        employeePhone = phone;
        employeeEmail = email;
        employeePayType = payType;
        employeeContractHours = contractHours;
        employeePayRate = payRate;
        employeeMaxHoliday = maxHoliday;
        employeeCurHoliday = curHoliday;
        employeeMaxSick = maxSick;
        employeeCurSick = curSick;
        employeePassword = password;
        employeeAccessLevel = accessLevel;
        employeeActive = active;
    }

    /*
    Employee class getters
     */
    public int getEmployeeID() {
        return employeeID;
    }

    public String getEmployeeFName() {
        return employeeFName;
    }

    public String getEmployeeSName() {
        return employeeSName;
    }

    public String getEmployeeDOB() {
        return employeeDOB;
    }

    public String getEmployeeAddress1() {
        return employeeAddress1;
    }

    public String getEmployeeAddress2() {
        return employeeAddress2;
    }

    public String getEmployeeCity() {
        return employeeCity;
    }

    public String getEmployeePostcode() {
        return employeePostcode;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public String getEmployeePayType() {
        return employeePayType;
    }

    public int getEmployeeContractHours() {
        return employeeContractHours;
    }

    public double getEmployeePayRate() {
        return employeePayRate;
    }

    public int getEmployeeMaxHoliday() {
        return employeeMaxHoliday;
    }

    public int getEmployeeCurHoliday() {
        return employeeCurHoliday;
    }

    public int getEmployeeMaxSick() {
        return employeeMaxSick;
    }

    public int getEmployeeCurSick() {
        return employeeCurSick;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public String getEmployeeAccessLevel() {
        return employeeAccessLevel;
    }

    public boolean isEmployeeActive() {
        return employeeActive;
    }

    /*
    * Employee class methods
     */
    /**
     * Method to add a new employee entry into the database based on passed
     * parameters. Refreshes employeeArray upon completion to include new
     * employee entry.
     *
     * @param fName
     * @param sName
     * @param DOB
     * @param address1
     * @param address2
     * @param city
     * @param postcode
     * @param phoneNo
     * @param email
     * @param payType
     * @param contractHours
     * @param payRate
     * @param maxHoliday
     * @param maxSick
     * @return
     */
    public static boolean addEmployee(String fName, String sName, String DOB,
            String address1, String address2, String city, String postcode,
            String phoneNo, String email, String payType, int contractHours,
            double payRate, int maxHoliday, int maxSick) {

        boolean done = false;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.EMPLOYEE);

            DBConnection.prepStatement.setString(1, fName);
            DBConnection.prepStatement.setString(2, sName);
            DBConnection.prepStatement.setString(3, DOB);
            DBConnection.prepStatement.setString(4, address1);
            DBConnection.prepStatement.setString(5, address2);
            DBConnection.prepStatement.setString(6, city);
            DBConnection.prepStatement.setString(7, postcode);
            DBConnection.prepStatement.setString(8, phoneNo);
            DBConnection.prepStatement.setString(9, email);
            DBConnection.prepStatement.setString(10, payType);
            DBConnection.prepStatement.setInt(11, contractHours);
            DBConnection.prepStatement.setDouble(12, payRate);
            DBConnection.prepStatement.setInt(13, maxHoliday);
            DBConnection.prepStatement.setInt(14, maxSick);

            int result = DBConnection.prepStatement.executeUpdate();

            if (result == 1) {
                done = true;
            }//if END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END

        DBConnection.getAllEmployees();

        return done;
    }//addEmployee() END

    /**
     * Method to update an employee entry into the database based on passed
     * parameters. Refreshes employeeArray upon completion to include updated
     * employee entry.
     *
     * @param fName
     * @param sName
     * @param DOB
     * @param address1
     * @param address2
     * @param city
     * @param postcode
     * @param phoneNo
     * @param email
     * @param payType
     * @param contractHours
     * @param payRate
     * @param maxHoliday
     * @param maxSick
     * @param employeeID
     * @return
     */
    public static boolean updateEmployeeDetails(String fName, String sName, String DOB,
            String address1, String address2, String city, String postcode,
            String phoneNo, String email, String payType, int contractHours,
            double payRate, int maxHoliday, int maxSick, int employeeID) {

        boolean done = false;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.EMPLOYEE);

            DBConnection.prepStatement.setString(1, fName);
            DBConnection.prepStatement.setString(2, sName);
            DBConnection.prepStatement.setString(3, DOB);
            DBConnection.prepStatement.setString(4, address1);
            DBConnection.prepStatement.setString(5, address2);
            DBConnection.prepStatement.setString(6, city);
            DBConnection.prepStatement.setString(7, postcode);
            DBConnection.prepStatement.setString(8, phoneNo);
            DBConnection.prepStatement.setString(9, email);
            DBConnection.prepStatement.setString(10, payType);
            DBConnection.prepStatement.setInt(11, contractHours);
            DBConnection.prepStatement.setDouble(12, payRate);
            DBConnection.prepStatement.setInt(13, maxHoliday);
            DBConnection.prepStatement.setInt(14, maxSick);
            DBConnection.prepStatement.setInt(15, employeeID);

            int result = DBConnection.prepStatement.executeUpdate();

            if (result == 1) {
                done = true;
            }//if END

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END

        DBConnection.getAllEmployees();

        return done;
    }//updateEmployeeDetails() END

    /**
     * Method to remove an employee from database. Sets 'Active' column to false
     * but leaves record intact to ensure referential integrity. Refreshes
     * employeeArray upon completion to remove employee.
     *
     * @param employeeID
     * @return
     */
    public static boolean removeEmployee(int employeeID) {
        boolean done = false;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLRemove.EMPLOYEE);

            DBConnection.prepStatement.setInt(1, employeeID);

            int result = DBConnection.prepStatement.executeUpdate();

            if (result == 1) {
                done = true;
            }

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }//try catch END

        DBConnection.getAllEmployees();

        return done;
    }//removeEmployee() END

    public static void increaseCurHoliday(int employeeID) {

        int index = employeeID - 1;

        int newCurHoliday = DBConnection.employeeArray.get(index).getEmployeeCurHoliday() + 1;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.APPROVED_HOLIDAY);

            DBConnection.prepStatement.setInt(1, newCurHoliday);
            DBConnection.prepStatement.setInt(2, employeeID);

            DBConnection.prepStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }

        DBConnection.getAllEmployees();
    }



}
