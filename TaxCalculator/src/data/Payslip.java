/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sql.SQLInsert;

/**
 *
 * @author ori16108422
 */
public class Payslip {

    private int payslipID;
    private int employeeID;
    private double totalHours;
    private double grossPay;
    private double nationalInsurance;
    private double taxBracket1;
    private double taxBracket2;
    private double taxBracket3;
    private double netPay;
    private String processedOn;

    public Payslip(int payslipID, int employeeID, double totalHours, double grossPay,
            double nationalInsurance, double taxBracket1, double taxBracket2,
            double taxBracket3, double netPay, String processedOn) {
        this.payslipID = payslipID;
        this.employeeID = employeeID;
        this.totalHours = totalHours;
        this.grossPay = grossPay;
        this.nationalInsurance = nationalInsurance;
        this.taxBracket1 = taxBracket1;
        this.taxBracket2 = taxBracket2;
        this.taxBracket3 = taxBracket3;
        this.netPay = netPay;
        this.processedOn = processedOn;
    }

    public int getPayslipID() {
        return payslipID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public double getTotalHours() {
        return totalHours;
    }

    public double getGrossPay() {
        return grossPay;
    }

    public double getNationalInsurance() {
        return nationalInsurance;
    }

    public double getTaxBracket1() {
        return taxBracket1;
    }

    public double getTaxBracket2() {
        return taxBracket2;
    }

    public double getTaxBracket3() {
        return taxBracket3;
    }

    public double getNetPay() {
        return netPay;
    }

    public String getProcessedOn() {
        return processedOn;
    }

    public static void addPayslip(int employeeID, double totalHours, int arrayPos) {

        double payRate = DBConnection.employeeArray.get(arrayPos).getEmployeePayRate();
        double grossPay;
        double annualPay;
        double nationalInsurance = 0;
        double taxBracket1 = 0;
        double taxBracket2 = 0;
        double taxBracket3 = 0;

        if (DBConnection.employeeArray.get(arrayPos).getEmployeePayType().equals("Hourly")) {
            double weeklyPay = DBConnection.employeeArray.get(arrayPos).getEmployeeContractHours() * payRate;

            grossPay = totalHours * payRate;
            annualPay = weeklyPay * 52;
        } else {
            grossPay = payRate / 12;
            annualPay = payRate;

        }

        if (annualPay > TaxBrackets.NATIONAL_INSURANCE_THRESHOLD
                & annualPay <= TaxBrackets.NATIONAL_INSURANCE_HIGHER_THRESHOLD) {

            double temp1 = annualPay - TaxBrackets.NATIONAL_INSURANCE_THRESHOLD;
            double temp2 = temp1 * TaxBrackets.NATIONAL_INSURANCE_PERCENT;

            nationalInsurance = temp2 / 12;
        } else if (annualPay > TaxBrackets.NATIONAL_INSURANCE_HIGHER_THRESHOLD) {

            double temp1 = 41392 * TaxBrackets.NATIONAL_INSURANCE_PERCENT;

            double temp2 = annualPay - TaxBrackets.NATIONAL_INSURANCE_HIGHER_THRESHOLD;
            double temp3 = temp2 * TaxBrackets.NATIONAL_INSURANCE_HIGHER_PERCENT;

            double temp4 = temp1 + temp3;
            nationalInsurance = temp4 / 12;
        }

        if (annualPay > TaxBrackets.PERSONAL_ALLOWANCE_THRESHOLD
                & annualPay < TaxBrackets.BASIC_RATE_THRESHOLD) {
            double temp1 = annualPay - TaxBrackets.PERSONAL_ALLOWANCE_THRESHOLD;
            double temp2 = temp1 * TaxBrackets.BASIC_RATE_PERCENT;
            taxBracket1 = temp2 / 12;
        } else if (annualPay > TaxBrackets.BASIC_RATE_THRESHOLD
                & annualPay < TaxBrackets.HIGHER_RATE_THRESHOLD) {
            double temp1 = 37500 * TaxBrackets.BASIC_RATE_PERCENT;
            taxBracket1 = temp1 / 12;

            double temp2 = annualPay - TaxBrackets.BASIC_RATE_THRESHOLD;
            double temp3 = temp2 * TaxBrackets.HIGHER_RATE_PERCENT;
            taxBracket2 = temp3 / 12;

        } else if (annualPay > TaxBrackets.HIGHER_RATE_THRESHOLD) {
            double temp1 = 37500 * TaxBrackets.BASIC_RATE_PERCENT;
            taxBracket1 = temp1 / 12;

            double temp2 = 100000 * TaxBrackets.HIGHER_RATE_PERCENT;
            taxBracket2 = temp2 / 12;

            double temp3 = annualPay - TaxBrackets.HIGHER_RATE_THRESHOLD;
            double temp4 = temp3 * TaxBrackets.ADDITIONAL_RATE_PERCENT;
            taxBracket3 = temp4 / 12;
        }

        double netPay = grossPay - nationalInsurance - taxBracket1 - taxBracket2 - taxBracket3;
        
        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.PAYSLIP);
            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.setDouble(2, totalHours);
            DBConnection.prepStatement.setDouble(3, grossPay);
            DBConnection.prepStatement.setDouble(4, nationalInsurance);
            DBConnection.prepStatement.setDouble(5, taxBracket1);
            DBConnection.prepStatement.setDouble(6, taxBracket2);
            DBConnection.prepStatement.setDouble(7, taxBracket3);
            DBConnection.prepStatement.setDouble(8, netPay);
            DBConnection.prepStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Payslip.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
