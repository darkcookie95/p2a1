/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import static data.DBConnection.employeeArray;
import java.sql.SQLException;
import java.time.Duration;
import java.util.*;
import sql.SQLInsert;
import sql.SQLUpdate;

/**
 *
 * @author ori16108422
 */
public class ClockIn {

    private int clockID;
    private int employeeID;
    private String clockIn;
    private String clockOut;
    private boolean processed;

    public ClockIn(int clockID, int employeeID, String clockIn, String clockOut, boolean processed) {
        this.clockID = clockID;
        this.employeeID = employeeID;
        this.clockIn = clockIn;
        this.clockOut = clockOut;
        this.processed = processed;
    }

    public int getClockID() {
        return clockID;
    }

    public void setClockID(int clockID) {
        this.clockID = clockID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOut() {
        return clockOut;
    }

    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public static boolean startShift(int employeeID) {
        boolean done = false;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.CLOCK_IN);

            DBConnection.prepStatement.setInt(1, employeeID);

            int result = DBConnection.prepStatement.executeUpdate();

            if (result == 1) {
                done = true;
            }
        } catch (SQLException sqlex) {

        }

        return done;
    }

    public static boolean endShift(int employeeID) {
        boolean done = false;

        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.CLOCK_OUT);

            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.setInt(2, employeeID);

            int result = DBConnection.prepStatement.executeUpdate();

            if (result == 1) {
                done = true;
            }
        } catch (SQLException sqlex) {

        }
        return done;
    }

    public static void calculateTimeWorked() {
        double workTime;
        double hours = 60.0;

        for (int index = 0; index < DBConnection.employeeArray.size(); index++) {

            workTime = 0.00;
            DBConnection.getIndividualClockIn(employeeArray.get(index).getEmployeeID());

            for (int count = 0; count < DBConnection.clockInArray.size(); count++) {

                java.util.Date timeOne = new Date(DBConnection.clockInArray.get(count).getClockIn());
                java.util.Date timeTwo = new Date(DBConnection.clockInArray.get(count).getClockOut());

                Duration duration = Duration.between(timeOne.toInstant(), timeTwo.toInstant());
                System.out.println(duration.toMinutes());
                workTime += duration.toMinutes() / hours;

                processClockIn(employeeArray.get(index).getEmployeeID());
            }
            System.out.println(workTime);

            Payslip.addPayslip(employeeArray.get(index).getEmployeeID(), workTime, index);

        }
    }

    public static void processClockIn(int employeeID) {
        try {

            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLUpdate.PROCESS_CLOCK);
            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.executeUpdate();

        } catch (Exception ex) {

        }
    }

}
