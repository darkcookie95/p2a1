/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;
import java.sql.SQLException;
import java.sql.Time;
import java.time.Duration;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sql.SQLInsert;
import sql.SQLRemove;

/**
 *
 * @author ori16108422
 */
public class Rota {
    private int rotaID;
    private int employeeID;
    private String shiftDate;
    private String startTime;
    private String endTime;


    public Rota(int rotaID, int employeeID, String shiftDate, String startTime, String endTime) {
        this.rotaID = rotaID;
        this.employeeID = employeeID;
        this.shiftDate = shiftDate;
        this.startTime = startTime;
        this.endTime = endTime;

    }

    public int getRotaID() {
        return rotaID;
    }

    public void setRotaID(int rotaID) {
        this.rotaID = rotaID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    public static void addNewRota(int employeeID, String shiftDate, String startTime, String endTime){
        
        
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLInsert.ROTA);
            
            DBConnection.prepStatement.setInt(1, employeeID);
            DBConnection.prepStatement.setString(2, shiftDate);
            DBConnection.prepStatement.setString(3, startTime);
            DBConnection.prepStatement.setString(4, endTime);
            
            DBConnection.prepStatement.executeUpdate();
            
            DBConnection.getAllRota();
        } catch (SQLException ex) {
            Logger.getLogger(Rota.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    public static void removeRota(int ID){
        try{
            DBConnection.prepStatement = DBConnection.connection.prepareStatement(SQLRemove.ROTA);
            
            DBConnection.prepStatement.setInt(1, ID);
            
            DBConnection.prepStatement.executeUpdate();
            
            DBConnection.getAllRota();
            
        } catch (SQLException ex) {
            Logger.getLogger(Rota.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
